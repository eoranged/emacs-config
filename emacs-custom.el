(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(elpy-modules (quote (elpy-module-company elpy-module-eldoc)))
 '(enable-local-variables t)
 '(flycheck-flake8-error-level-alist
   (quote
    (("^E9.*$" . error)
     ("^F82.*$" . error)
     ("^F83.*$" . error)
     ("^D.*$" . info)
     ("^N.*$" . info)
     ("^T002$" . error)
     ("^T001$" . info)
     ("^I.*$" . info))))
 '(magit-commit-arguments (quote ("--gpg-sign=0E35E7F9DEFDAEB8")))
 '(magit-fetch-arguments (quote ("--prune")))
 '(magit-rebase-arguments (quote ("--autostash" "--interactive")))
 '(package-selected-packages
   (quote
    (ibuffer-projectile load-dir req-package yaml-mode wgrep-ag vue-mode tern-auto-complete stickyfunc-enhance smex smali-mode skewer-mode sass-mode racer python-mode pyimpsort pyenv-mode py-isort pug-mode projectile pony-mode po-mode pinentry org-jira markdown-mode magit lua-mode json-mode ido-ubiquitous header2 haskell-mode go-mode go-autocomplete glsl-mode flymake-sass flycheck-rust flx-ido elpy diminish company-racer company-flx clipmon autopair applescript-mode ag)))
 '(safe-local-variable-values
   (quote
    ((eor/project/python-path . "/Users/vprotasov/dev/web/Sources/build")
     (python-shell-virtualenv-root . "/Users/vprotasov/dev/web/Sources/build/venv")
     (header-auto-update-enabled)))))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )
