;;; init --- my init file

;;; Commentary:

;;  Requires at least Emacs 24.3 to work properly.
;;  It's better to use latest stable release:
;;  I'm trying to keep all my Emacs installations up to date.

;;; Code:

;; Do not show useless buffers on startup

;; Added by Package.el.  This must come before configurations of
;; installed packages.  Don't delete this line.  If you don't want it,
;; just comment it out by adding a semicolon to the start of the line.
;; You may delete these explanatory comments.
(package-initialize)

(setq inhibit-splash-screen t
      inhibit-startup-echo-area-message t)

;; Hide all useless stuff
(tool-bar-mode 0)
;;(menu-bar-mode 0)
(scroll-bar-mode 0)

(setenv "LC_ALL" "en_US.UTF-8")
;; Extend path variable and exec-path
(when (eq system-type 'darwin)
  (setenv "PATH" (concat (getenv "PATH") ":/usr/local/bin"))
                                        ; TODO add as a last element
  (add-to-list 'exec-path "/usr/local/bin"))

;; extend python path
(setenv "PYTHONPATH" (concat (getenv "PYTHONPATH") ":/Users/vprotasov/dev/web/Sources/build:/Users/vprotasov/dev/web/Sources/build/venv/lib/python2.6/site-packages"))

(setenv "GOPATH" (expand-file-name "~/.local/go/"))
(add-to-list 'exec-path (expand-file-name "~/.local/go/bin"))

(defun eor/concat-list (lst separator)
  "A non-recursive function that concatenates a list of strings."
  (when (listp lst)
      (let ((result ""))
        (dolist (item lst)
          (if (stringp item)
              (if (and (stringp separator) (not (eq result "")))
                  (setq result (concat result separator item))
                (setq result (concat result item)))
            (prin1 item)))
        result)))

;; load settings from json file
;; (let*
;;     ((json-object-type 'hash-table)
;;      (options (json-read-file "~/dev/web/settings.json")))
;;   (setq py-isort-options (append (gethash "py-isort-options" options) nil)))


;;
;; package.el
;;
(require 'package)
(add-to-list 'package-archives
             '("melpa" . "http://melpa.org/packages/") t)
(add-to-list 'package-directory-list (expand-file-name "elpa-to-submit" user-emacs-directory))
(package-initialize)
(when (not package-archive-contents)
  (package-refresh-contents))

(defvar eor/required-packages
  (list 'ag
        'autopair
        'company
        'company-flx
        'diminish
        'elpy
        'f
        'flx
        'flx-ido
        'flycheck
        'magit
        'haskell-mode
        'header2
        'org-jira
        'projectile
        'py-isort
        'pyenv-mode
        'python-mode
        's
        'smex
        'wgrep
        'wgrep-ag
        'yasnippet)
  "Libraries that should be installed by default.")

(dolist (package eor/required-packages)
  (unless (or (member package package-activated-list)
              (functionp package))
    (message "Installing %s" (symbol-name package))
    (package-install package)))

;;
;; Freeing keys
;;
(global-unset-key (kbd "C-z"))      ;; suspend-emacs
;;(global-unset-key (kbd "C-q"))      ;; quoted-insert
(global-unset-key (kbd "C-x C-r"))  ;; ido-find-file-read-only
(global-unset-key (kbd "C-t"))      ;; transpose-chars
(global-unset-key (kbd "M-c"))      ;; capitalize-word  - often pressed by mistake
(global-unset-key (kbd "s-n"))      ;; make-frame - annoying


(when (eq system-type 'darwin) ;; mac specific settings
  (setq mac-option-modifier 'meta)
  (setq mac-command-modifier 'super)
  (global-set-key [kp-delete] 'delete-char) ;; sets fn-delete to be right-delete
  )


;;
;; Generally useful packages
;;
(require 'f)

;;
;; Custom settings file
;;
(setq custom-file (concat user-emacs-directory "emacs-custom.el"))
;; Load custom file only if one is exists
(if (file-exists-p custom-file)
    (load custom-file))

;; Data directory
(defconst emacs-persistence-directory (concat user-emacs-directory "data/")
  "Directory for Emacs data files and other garbage.")

(unless (file-exists-p emacs-persistence-directory)
  (make-directory emacs-persistence-directory t))

;;
;; auto-save and auto-backup
;;
(require 'desktop)
(setq-default desktop-dirname (expand-file-name "desktop/" emacs-persistence-directory))
(setq-default desktop-base-file-name "emacs.desktop")
(setq-default desktop-base-lock-name "emacs.desktop.lock")
(setq desktop-path (list desktop-dirname))
(desktop-save-mode t)

(setq auto-save-list-file-prefix (concat emacs-persistence-directory "auto-save-list/saves-"))
(setq make-backup-files nil)
(defconst eor/autosave-directory (f-join emacs-persistence-directory "autosave"))
(unless (f-dir? eor/autosave-directory)
  (make-directory eor/autosave-directory))
(setq auto-save-file-name-transforms
      `((".*" ,eor/autosave-directory t)))


;; Kill stale buffers each day at midnight
;; https://www.gnu.org/software/emacs/manual/html_node/emacs/Kill-Buffer.html
(midnight-mode t)
(add-hook 'midnight-hook 'clean-buffer-list)

;;
;; Start server to allow connections from emacsclient
;;
(require 'server)
(unless (server-running-p)
  (server-start))

;;
;; Other useful stuff
;;

;; log recent files (recentf-open-files will list them all)
(recentf-mode t)
(setq-default recentf-save-file (f-join emacs-persistence-directory "recentf"))
;; highlight current line
(global-hl-line-mode t)
;; save minibuffer history
(savehist-mode 1)
(setq-default savehist-file (f-join emacs-persistence-directory "minibuffer.history"))
;; make indentation commands use space only (never tab character)
(setq-default indent-tabs-mode nil)
;; set default tab char's display width to 4 spaces
(setq-default tab-width 4)
;; Show column numbers
(column-number-mode t)
;; And matching parens
(show-paren-mode t)
(require 'paren)
(setq blink-matching-paren-distance nil)
(setq show-paren-style 'mixed)


;; Disable the alarm
(setq ring-bell-function 'ignore)

;; Kill more, kill better
(global-set-key (kbd "C-k") 'kill-whole-line)
(global-set-key (kbd "C-S-k") 'kill-line)
(global-set-key (kbd "s-k") 'kill-this-buffer)
(global-set-key (kbd "s-s") 'save-buffer)


;; Make eshell store its files in .emacs.d
(require 'eshell)
(setq eshell-directory-name (concat emacs-persistence-directory "eshell"))
;; Explicitly set temporary folder
(setq temporary-file-directory (expand-file-name "~/.tmp"))
(unless (file-accessible-directory-p temporary-file-directory)
  (make-directory temporary-file-directory))

;; delete trailing spaces before saving
(add-hook 'before-save-hook 'delete-trailing-whitespace)

;; dark theme
(load-theme 'tsdh-dark)
;; smaller font
(set-frame-font "Monaco-10")
;; (set-face-attribute 'default nil :weight 'normal)


;; ibuffer
(require 'ibuffer)
(global-set-key (kbd "C-x C-b") 'ibuffer-other-window)
(global-set-key (kbd "s-b") 'ido-switch-buffer)
(setq ibuffer-expert t)
(require 'ibuf-ext)
(setq ibuffer-show-empty-filter-groups nil)
(setq ibuffer-saved-filter-groups
      '(("work"
         ;; Parallels web
         ("|| Desktop" (filename . ".*/web/Sources/Desktop/.*"))
         ("|| LS" (filename . ".*/web/Sources/LicensingService/.*"))
         ("|| Account" (filename . ".*/web/Sources/MyAccount/.*"))
         ("|| RAS" (filename . ".*/web/Sources/RAS/.*"))
         ("|| Toolbox" (filename . ".*/web/Sources/PTB/.*"))
         ("|| PMM" (filename . ".*/web/Sources/PMM/.*"))
         ("|| Common" (filename . ".*/web/Sources/Common/Backend/.*"))
         ("|| SWP" (filename . ".*/work/swp_backend/.*"))
         ("|| Frontend" (filename . ".*/work/my_frontend/.*"))
         ("|| Other" (filename . ".*/web/Sources/.*"))
         ;; Cookbooks
         ("|| Cookbooks" (filename . ".*/work/env.*"))
         ;; Emacs configuration
         ("emacs.d" (or (filename . ".emacs.d")
                        (filename . "emacs-config")
                        (filename . ".*/.*.el$")))
         ;; Org-mode files
         ("org" (mode . org-mode))
         ;; Other stuff
         ("dired" (mode . dired-mode))
         ("magit" (name . "\*magit"))
         ("ag" (name . "\*ag"))
         ("logs" (filename . ".*\.log$"))
         ("*..*" (name . "\*.*\*"))
         )))

(add-hook 'ibuffer-mode-hook
          '(lambda ()
             (ibuffer-switch-to-saved-filter-groups "work")
             (add-to-list 'ibuffer-never-show-predicates "TAGS")
             (add-to-list 'ibuffer-never-show-predicates ".*org_archive")
             (ibuffer-auto-mode 1)))


;;
;; Ido
;;
(require 'ido)
(ido-mode 1)
(ido-everywhere 1)
;; flx completion
(require 'flx-ido)
(flx-ido-mode 1)
;; disable ido faces to see flx highlights.
(setq ido-enable-flex-matching t)
(setq ido-use-faces nil)
;; Ido ubiquitous
(require 'ido-completing-read+)
(ido-ubiquitous-mode 1)
;; smex
(global-set-key (kbd "M-x") 'smex)
(global-set-key (kbd "s-x") 'smex-major-mode-commands)
;; annoying prompts
(fset 'yes-or-no-p 'y-or-n-p)
(setq confirm-nonexistent-file-or-buffer nil)
(setq ido-create-new-buffer 'always)
;; move ido.last to data directory
(setq ido-save-directory-list-file (concat emacs-persistence-directory "ido.last"))
;;
;; Projectile
;;
(require 'projectile)
(defconst projectile-data-dir (f-join emacs-persistence-directory "projectile/"))
(setq projectile-known-projects-file (expand-file-name "bookmarks.eld" projectile-data-dir))
(setq projectile-cache-file (expand-file-name "projectile.cache" projectile-data-dir))
(define-key projectile-mode-map projectile-keymap-prefix nil)
(define-key projectile-mode-map (kbd "s-p") #'projectile-command-map)
(projectile-mode t)
(defun eor/projectile-switch-project-action ()
  "Adjust environment settings when switching project."
  (setenv "GOPATH" (projectile-project-root))
  )
(setq projectile-switch-project-action 'eor/projectile-switch-project-action)

(defcustom eor/project/repo-browser-url-pattern nil
  "Pattern to use to open file in browser."
  :type '(choice (const nil) string)
  :safe 'stringp
  :group 'eoranged)

(defun eor/project/file-name ()
  "Return file name relative to project root."
  (when (projectile-project-p)
    (s-chop-prefix (projectile-project-root) buffer-file-name)))

(defun eor/project/copy-file-name ()
  "Copy file name relative to project root."
  (interactive)
  (when (projectile-project-p)
    (kill-new (eor/project/file-name))))

(defun eor/project/get-repo-file-url ()
  "Open current file in browser (for github, gitlab, stash and so on)."
  (when (and (boundp 'eor/project/repo-browser-url-pattern)
             (projectile-project-p))

    (let ((filename (eor/project/file-name))
          (revision (vc-working-revision (buffer-file-name))))
      (s-replace "{filename}" filename
                 (s-replace "{revision}" revision
                            eor/project/repo-browser-url-pattern)))))

(defun eor/project/open-repo-file-in-browser ()
  "Open current file in repo browser."
  (interactive)
  (let ((url (eor/project/get-repo-file-url)))
    (when url
      (browse-url url))))

;; Mode-line
(setq-default mode-line-format
              (list
               "   "
               ;; is this buffer read-only?
               '(:eval (if buffer-read-only
                           (propertize "✎ "
                                       'face '(:foreground "red" :height 70)
                                       'help-echo "Buffer is read-only")
                         (propertize "✎ "
                                     'face '(:foreground "green" :height 70)
                                     'help-echo "Buffer is writable")))
               ;; was this buffer modified since the last save?
               '(:eval (if (buffer-modified-p)
                           (propertize "★ "
                                       'face '(:height 70 :foreground "red")
                                       'help-echo "Buffer has been modified")
                         (propertize "★ "
                                     'face '(:height 70 :foreground "green")
                                     'help-echo "Buffer is saved")))
               "    "
               ;; the buffer name; the file name as a tool tip
               '(:eval (propertize "%b "
                                   'face '(:weight bold)
                                   'help-echo (buffer-file-name)))

               "    "

               mode-line-position

               "    "

               '(vc-mode vc-mode)
               ))


;;
;; Clipmon
;;
(add-to-list 'after-init-hook 'clipmon-mode-start)
(add-to-list 'after-init-hook 'clipmon-persist)

(setq clipmon-timer-interval 0.5)       ; check system clipboard every n secs
(setq clipmon-autoinsert-sound nil)     ; t for included beep, or path or nil
(setq clipmon-autoinsert-color "red") ; color of cursor when autoinsert is on
(setq clipmon-autoinsert-timeout 5)   ; stop autoinsert after n mins inactivity

(global-set-key (kbd "s-y") 'clipmon-autoinsert-toggle)

;;
;; Magit
;;
(require 'magit)
(global-set-key (kbd "s-m") 'magit-status)
(setq
 ;; use ido to look for branches
 magit-completing-read-function 'magit-ido-completing-read
 )
;; For commit signing to work
(pinentry-start)
;;
;; Flycheck
;;
(require 'flycheck)
(add-hook 'after-init-hook #'global-flycheck-mode)
(eval-after-load "flycheck" '(diminish 'flycheck-mode))

;;
;; Flyspell
;;
(require 'flyspell)
(add-hook 'prog-mode-hook #'flyspell-prog-mode)
(setq ispell-program-name "aspell")
;; Enable if using aspell and it is slow
;;(setq ispell-extra-args '("--sug-mode=fast"))
(global-set-key (kbd "C-z c") 'flyspell-correct-word-before-point)

;;
;; swiper
;;
;;(ivy-mode 1)
;;(setq ivy-use-virtual-buffers t)
;;(global-set-key "\C-q\C-s" 'swiper)

;;
;; The silver searcher
;;
(require 'ag)
;; Rename ag buffers for easier access
(defun ag/buffer-name (search-string directory regexp)
  "Return a buffer name formatted according to ag.el conventions."
  (format "*ag: %s (%s)*" search-string directory))

;;
;; Yasnippet
;;
(yas-global-mode 1)
(diminish 'yas-minor-mode)

;;
;; Smali
;;
(autoload 'smali-mode "smali-mode" "Major mode for editing and viewing smali issues" t)
(add-to-list 'auto-mode-alist '(".smali$" . smali-mode))


;;
;; Python
;;
(require 'python)
(add-to-list 'auto-mode-alist '("\\.py.example\\'" . python-mode))
;;
;; completion and code navigation
;;
(require 'elpy)
;; redefining this function for correct virtualenv handling
(defcustom eor/project/python-path nil
  "Additional values to add to PYTHONPATH for the current project."
  :type '(choice (const nil) string)
  :safe 'stingp
  :group 'eoranged)

;; (defun elpy-rpc--get-rpc-buffer ()
;;   "Return the RPC buffer associated with the current buffer, creating one if necessary."
;;   (when (not (elpy-rpc--process-buffer-p elpy-rpc--buffer))
;;     (let ((project-python-command elpy-rpc-python-command)
;;           (project-python-path nil))
;;       (when (and (boundp 'python-shell-virtualenv-path) (stringp 'python-shell-virtualenv-path))
;;         (setq python-command (expand-file-name "bin/python" python-shell-virtualenv-path)))
;;       (when (stringp 'eor/project/python-path)
;;         (setq project-python-path eor/project/python-path))
;;       (setq elpy-rpc--buffer
;;           (or (elpy-rpc--find-buffer (elpy-library-root)
;;                                      project-python-command)
;;               (elpy-rpc--open (elpy-library-root)
;;                               project-python-command
;;                               project-python-path)))))
;;   elpy-rpc--buffer)

(add-hook 'python-mode-hook
          #'(lambda () (elpy-enable)))
(add-hook 'python-mode-hook
          (lambda () (define-key python-mode-map (kbd "s-i") 'elpy-importmagic-add-import)))
(add-hook 'python-mode-hook
          (lambda () (define-key python-mode-map (kbd "s-I") 'elpy-importmagic-fixup)))


;; import sorting (isort)
(require 'py-isort)
;; django support
(require 'pony-mode)

(defun pony-project-root()
  "Return the root of the project(dir with manage.py in) or nil"
  (pony-localise
   'pony-this-project-root
   '(lambda ()
      (let ((curdir default-directory)
            (max 10)
            (found nil))
        (while (and (not found) (> max 0))
          (progn
            (if (or (file-exists-p (concat curdir "/bin/django")) ; Buildout?
                    (file-exists-p (concat curdir "manage.py"))
                    (file-exists-p (concat curdir "dev_manage.py")))
                (progn
                  (setq found t))
              (progn
                (setq curdir (concat curdir "../"))
                (setq max (- max 1))))))
        (if found (expand-file-name curdir))))))

(defun pony-manage-cmd()
  "Return the current manage command
This command will only work if you run with point in a buffer that is within your project"
  (let ((found nil)
        (virtualenv '../bin/activate)
        (cmds (list 'bin/django '../bin/django 'manage.py 'dev_manage.py)))
    (if (pony-rooted-sym-p virtualenv)
        (expand-file-name (concat (pony-project-root) "manage.py"))
      ;; Otherwise, look for buildout, defaulting to the standard manage.py script
      (progn
        (dolist (test cmds)
          (if (and (not found) (pony-rooted-sym-p test))
              (setq found (expand-file-name
                           (concat (pony-project-root) (symbol-name test))))))
        (if found
            found)))))

;;
;; Rust
;;
(setq racer-cmd (expand-file-name "~/.cargo/bin/racer"))
(setq racer-rust-src-path (expand-file-name "~/dev/stuff/rust/rust/src/"))
(setq company-racer-executable racer-cmd)
(setq company-racer-rust-src racer-rust-src-path)

(require 'racer)
(add-hook 'rust-mode-hook
          '(lambda ()
             ;; Enable racer
             (racer-activate)
             ;; Hook in racer with eldoc to provide documentation
             (racer-turn-on-eldoc)
             ;; Use flycheck-rust in rust-mode
             (add-hook 'flycheck-mode-hook #'flycheck-rust-setup)
             ;; Use company-racer in rust mode
             (set (make-local-variable 'company-backends) '(company-racer))
             ;; Key binding to jump to method definition
             (local-set-key (kbd "M-.") #'racer-find-definition)
             ;; Key binding to auto complete and indent
             ;;(local-set-key (kbd "TAB") #'racer-complete-or-indent)
             ))

;;
;; Company mode
;;
(require 'company)
(add-hook 'after-init-hook 'global-company-mode)
;; advising eply-module-company to use yasnippet
(defun eor/elpy-company-yasnippet (command &rest args)
  "Advice around elpy-module-company"
  (when (eq command `buffer-init)
    ;; company-backends is buffer-local variable by now, so just modifying it's value
    (setq company-backends (list
                            (list 'elpy-company-backend 'company-files :separate :with 'company-yasnippet)))
    ))
(advice-add 'elpy-module-company :after #'eor/elpy-company-yasnippet)

(with-eval-after-load 'company
  (setq-default company-minimum-prefix-length 1)
  (setq company-idle-delay 0.2)
  (company-flx-mode +1)
  (diminish 'company-mode))

;;
;; Autopair
;;
(require 'autopair)
(autopair-global-mode t)
(diminish 'autopair-mode)
(add-hook 'python-mode-hook
          #'(lambda ()
              (setq autopair-handle-action-fns
                    (list #'autopair-default-handle-action
                          #'autopair-python-triple-quote-action))))

;;
;; JS
;; https://truongtx.me/2014/02/23/set-up-javascript-development-environment-in-emacs
;;

(require 'js2-mode)
;; Enabling autocompletion
(add-hook 'js-mode-hook 'js2-minor-mode)
;; Better syntax highliting
(setq js2-highlight-level 3)
(add-hook 'js-mode-hook
          (lambda ()
            ;; Vue mode (looks like It is too lazy to load)
            (require 'vue-mode)
            ;; jshint
            (flycheck-mode t)
            ;; tern (completion and code navigation)
            (tern-mode t)))
;; disable jshint by default to prefer eslint
(setq-default flycheck-disabled-checkers
  (append flycheck-disabled-checkers
    '(javascript-jshint)))
;; tern as a completion source
(eval-after-load 'tern
   '(progn
      (require 'tern-auto-complete)
      (tern-ac-setup)))

;;
;; pug
;;
(add-hook 'pug-mode-hook
          (lambda ()
            (setq tab-width 2)))

;;
;; Go
;;
(defun my-go-mode-hook ()
  ;; ; Call Gofmt before saving
  ;; (add-hook 'before-save-hook 'gofmt-before-save)
  ;; ac-mode
  (auto-complete-mode 1)
  ; Godef jump key binding
  (local-set-key (kbd "M-.") 'godef-jump)
  (local-set-key (kbd "M-*") 'pop-tag-mark)
  )
(add-hook 'go-mode-hook 'my-go-mode-hook)
(with-eval-after-load 'go-mode
   (require 'go-autocomplete))
;;
;; header2
;;
(require 'header2)
;; should be set in .dir-locals file in project root
(defcustom eor/prl/project nil
  "Marker to detect Parallels project."
  :type '(choice (const nil) string)
  :safe 'stringp
  :group 'eoranged)

(defsubst eor/header-triple-quotes ()
  "Insert triple quotes."
  (insert "\"\"\"\n"))

(defsubst eor/header-python-no-prefix-string ()
  "Disable comment prefix for python files."
  (setq header-prefix-string ""))

(defsubst eor/prl/header-file-name ()
  "Insert \"File: \" line, using buffer's file name."
  (insert "File: "
          (if (buffer-file-name)
              (file-name-nondirectory (buffer-file-name))
            (buffer-name))
          "\n"))
(defsubst eor/prl/header-copyright-python ()
  "Insert copyright."
  (insert "Copyright:\n"
          "Copyright " (format-time-string "%Y-%Y. ") "Parallels International GmbH. All Rights Reserved.\n"))

(defsubst eor/prl/header-python ()
  "Insert docstring with copyright for python files in parallels projects."

  (when (and (boundp 'eor/prl/project) (eq major-mode 'python-mode))
    (eor/header-python-no-prefix-string)
    (eor/header-triple-quotes)
    (eor/prl/header-file-name)
    (header-blank)
    (eor/prl/header-copyright-python)
    (eor/header-triple-quotes)))

(setq make-header-hook '(eor/prl/header-python))



(defun eor/prl/header-update-copyright ()
  "Update copyright."
  (when (boundp 'eor/prl/project)
    (move-beginning-of-line nil)
    (search-forward "Copyright ")
    (forward-char 5)
    (delete-region (point) (progn (forward-word nil) (point)))
    (insert (format-time-string "%Y"))))
(register-file-header-action
 "Copyright [0-9][0-9][0-9][0-9]-[0-9][0-9][0-9][0-9]"
 'eor/prl/header-update-copyright)

(defun eor/prl/header-fix-company-name ()
  "Fix company name in copyright."
  (when (boundp 'eor/prl/project)
    (move-beginning-of-line nil)
    (when (search-forward "Parallels IP Holdings GmbH")
      (replace-match "Parallels International GmbH"))))
(register-file-header-action
 "Copyright [0-9][0-9][0-9][0-9]-[0-9][0-9][0-9][0-9]. Parallels IP Holdings GmbH"
 'eor/prl/header-fix-company-name)

(add-hook 'write-file-hooks 'auto-update-file-header)
(add-to-list 'hack-local-variables-hook 'auto-make-header)



;;
;; Switching layout when entering commands
;; http://ru-emacs.livejournal.com/82428.html
;;
(require 'quail)
(defun reverse-input-method (input-method)
  "Build the reverse mapping of single letters from INPUT-METHOD."
  (interactive
   (list (read-input-method-name "Use input method (default current): ")))
  (if (and input-method (symbolp input-method))
      (setq input-method (symbol-name input-method)))
  (let ((current current-input-method)
        (modifiers '(nil (control) (meta) (control meta))))
    (when input-method
      (activate-input-method input-method))
    (when (and current-input-method quail-keyboard-layout)
      (dolist (map (cdr (quail-map)))
        (let* ((to (car map))
               (from (quail-get-translation
                      (cadr map) (char-to-string to) 1)))
          (when (and (characterp from) (characterp to))
            (dolist (mod modifiers)
              (define-key local-function-key-map
                (vector (append mod (list from)))
                (vector (append mod (list to)))))))))
    (when input-method
      (activate-input-method current))))

(reverse-input-method 'russian-computer)

(defadvice read-passwd (around my-read-passwd act)
  "Don't change input method for password prompts."
  (let ((local-function-key-map nil))
    ad-do-it))

;; XML pretty print
;; http://stackoverflow.com/questions/12492/pretty-printing-xml-files-on-emacs#570049
(defun bf-pretty-print-xml-region (begin end)
  "Pretty format XML markup in region. You need to have nxml-mode
http://www.emacswiki.org/cgi-bin/wiki/NxmlMode installed to do
this.  The function inserts linebreaks to separate tags that have
nothing but whitespace between them.  It then indents the markup
by using nxml's indentation rules."
  (interactive "r")
  (save-excursion
      (nxml-mode)
      (goto-char begin)
      (while (search-forward-regexp "\>[ \\t]*\<" nil t)
        (backward-char) (insert "\n"))
      (indent-region begin end))
    (message "Ah, much better!"))


;;
;; undisabled commands
;;
(put 'downcase-region 'disabled nil)
(put 'upcase-region 'disabled nil)

(provide 'init)
;;; init.el ends here
